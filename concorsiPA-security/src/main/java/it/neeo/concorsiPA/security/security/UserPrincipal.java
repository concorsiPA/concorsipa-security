package it.neeo.concorsiPA.security.security;

import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.fasterxml.jackson.annotation.JsonIgnore;

import it.neeo.concorsiPA.security.model.User;
import it.neeo.concorsiPA.security.model.UserRole;

public class UserPrincipal implements UserDetails {
	/**
	 * 
	 */
	private static final long serialVersionUID = 2741028911403370575L;

	private String username;

	@JsonIgnore
	private String password;

	private String screenname;

	private String cf;

	@JsonIgnore
	private String email;

	private Collection<? extends GrantedAuthority> authorities;

	public UserPrincipal(String username, String password, String screenname, String cf, String email,
			Collection<? extends GrantedAuthority> authorities) {
		this.username = username;
		this.password = password;
		this.screenname = screenname;
		this.cf = cf;
		this.email = email;

		this.authorities = authorities;
	}

	public static UserPrincipal create(User user, UserRole currentUserRole) {
		List<GrantedAuthority> authorities = currentUserRole.getAuthRoleEntries().stream().collect(Collectors.toList());

		return new UserPrincipal(user.getUsername(), user.getPassword(), user.getScreenname(), user.getCf(),
				user.getEmail(), authorities);
	}

	@Override
	public String getUsername() {
		return username;
	}

	@Override
	public String getPassword() {
		return password;
	}

	public String getScreenname() {
		return screenname;
	}

	public String getCf() {
		return cf;
	}

	public String getEmail() {
		return email;
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return authorities;
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		UserPrincipal that = (UserPrincipal) o;
		return Objects.equals(username, that.username);
	}

	@Override
	public int hashCode() {

		return Objects.hash(username);
	}
}