package it.neeo.concorsiPA.security.controller;

import java.net.URI;
import java.util.Collections;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import it.neeo.concorsiPA.security.exception.AppException;
import it.neeo.concorsiPA.security.model.Role;
import it.neeo.concorsiPA.security.model.User;
import it.neeo.concorsiPA.security.payload.ApiResponse;
import it.neeo.concorsiPA.security.payload.JwtAuthenticationResponse;
import it.neeo.concorsiPA.security.payload.LoginRequest;
import it.neeo.concorsiPA.security.payload.SignUpRequest;
import it.neeo.concorsiPA.security.repository.RoleRepository;
import it.neeo.concorsiPA.security.repository.UserRepository;
import it.neeo.concorsiPA.security.security.JwtTokenProvider;

@RestController
@RequestMapping("/api/auth")
public class AuthController {

	@Autowired
	AuthenticationManager authenticationManager;

	@Autowired
	UserRepository userRepository;

	@Autowired
	RoleRepository roleRepository;

	@Autowired
	PasswordEncoder passwordEncoder;

	@Autowired
	JwtTokenProvider tokenProvider;

	@PostMapping("/signin")
	public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {

		Authentication authentication = authenticationManager.authenticate(
				new UsernamePasswordAuthenticationToken(loginRequest.getUsernameOrEmail(), loginRequest.getPassword()));

		SecurityContextHolder.getContext().setAuthentication(authentication);

		String jwt = tokenProvider.generateToken(authentication);
		return ResponseEntity.ok(new JwtAuthenticationResponse(jwt));
	}

	@PostMapping("/signup")
	public ResponseEntity<?> registerUser(@Valid @RequestBody SignUpRequest signUpRequest) {
		if (userRepository.existsByUsername(signUpRequest.getUsername())) {
			return new ResponseEntity(new ApiResponse(false, "Username is already taken!"), HttpStatus.BAD_REQUEST);
		}

		if (userRepository.existsByEmail(signUpRequest.getEmail())) {
			return new ResponseEntity(new ApiResponse(false, "Email Address already in use!"), HttpStatus.BAD_REQUEST);
		}
System.out.println(signUpRequest.getPassword());
		// Creating user's account
		User user = new User(signUpRequest.getUsername(), signUpRequest.getPassword(), signUpRequest.getCf(), signUpRequest.getCf(),
				signUpRequest.getEmail());

		user.setPassword(passwordEncoder.encode(user.getPassword()));
		user.setUtins("admin");
		System.out.println(signUpRequest.getPassword());

		Role userRole = roleRepository.findByCode("R_ADMIN")
				.orElseThrow(() -> new AppException("User Role not set."));

		//user.setRoles(Collections.singleton(userRole));

		User result = userRepository.save(user);

		URI location = ServletUriComponentsBuilder.fromCurrentContextPath().path("/api/users/{username}")
				.buildAndExpand(result.getUsername()).toUri();

		return ResponseEntity.created(location).body(new ApiResponse(true, "User registered successfully"));
	}
}