package it.neeo.concorsiPA.security.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

/**
 * 
 * @author enrico.sanna Creation date: 26 Apr 2019 Project: concorsiPA-security
 */
@Entity
@Table(name = "si_authkeys")
public class AuthKey extends BaseAuditEntity {
	private static final long serialVersionUID = 3179098054115274152L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "authority_id")
	private long authorityId;

	@NotBlank
	@Size(max = 100)
	private String name;

	@NotBlank
	@Size(max = 500)
	private String description;

	@NotBlank
	@Size(max = 20)
	private AuthKeyType authKeyType;

	@NotBlank
	private String authModule;

	private String className;

	private String methodName;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "authority_parent")
	AuthKey authParent;

	@OneToMany(fetch = FetchType.LAZY)
	@JoinColumn(name = "authority_id")
	private Set<AuthRoleEntry> authRoleEntries = new HashSet<>();

	public AuthKey(long authorityId, @NotBlank @Size(max = 100) String name,
			@NotBlank @Size(max = 500) String description) {
		super();
		this.authorityId = authorityId;
		this.name = name;
		this.description = description;
	}

	public AuthKey() {
		super();
	}

	public long getAuthorityId() {
		return authorityId;
	}

	public void setAuthorityId(long authorityId) {
		this.authorityId = authorityId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public AuthKeyType getAuthKeyType() {
		return authKeyType;
	}

	public void setAuthKeyType(AuthKeyType authKeyType) {
		this.authKeyType = authKeyType;
	}

	public String getAuthModule() {
		return authModule;
	}

	public void setAuthModule(String authModule) {
		this.authModule = authModule;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public String getMethodName() {
		return methodName;
	}

	public String getClassAndMethodName() {
		return className + "::" + methodName;
	}

	public void setMethod_name(String methodName) {
		this.methodName = methodName;
	}

	public AuthKey getAuthParent() {
		return authParent;
	}

	public void setAuthParent(AuthKey authParent) {
		this.authParent = authParent;
	}

	public Set<AuthRoleEntry> getAuthRoleEntries() {
		return authRoleEntries;
	}

	public void setAuthRoleEntries(Set<AuthRoleEntry> authRoleEntries) {
		this.authRoleEntries = authRoleEntries;
	}

}
