package it.neeo.concorsiPA.security.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import it.neeo.concorsiPA.security.model.Role;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {

	Optional<Role> findByCode(String code);

	List<Role> findByDescription(String description);

	List<Role> findByRoleIdIn(List<Long> role_ids);

	List<Role> findByCodeIn(List<String> roleCodes);
	
	Boolean existsByCode(String code);

}