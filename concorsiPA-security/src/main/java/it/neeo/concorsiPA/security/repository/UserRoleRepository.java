package it.neeo.concorsiPA.security.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import it.neeo.concorsiPA.security.model.Role;
import it.neeo.concorsiPA.security.model.User;
import it.neeo.concorsiPA.security.model.UserRole;

public interface UserRoleRepository extends JpaRepository<UserRole, Long>{

	List<UserRole> findByIdIn(List<Long> ids);

	List<UserRole> findByRoleIn(List<Role> roles);

	Optional<UserRole> findByRole(Role role);

	Optional<UserRole> findByUser(User user);

}
