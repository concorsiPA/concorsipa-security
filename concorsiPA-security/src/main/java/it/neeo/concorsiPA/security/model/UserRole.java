package it.neeo.concorsiPA.security.model;

import java.time.Instant;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import org.hibernate.annotations.NaturalId;

/**
 * 
 * @author enrico.sanna Creation date: 26 Apr 2019 Project: concorsiPA-security
 */
@Entity
@Table(name = "si_userroles")
public class UserRole extends BaseAuditEntity {

	private static final long serialVersionUID = -4252537594572416330L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "userrole_id")
	private long id;

	@NaturalId(mutable = true)
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "role_id")
	private Role role;

	@NaturalId(mutable = true)
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "username")
	@Size(max = 50)
	private User user;

	private Instant disabledDate;

	@NotBlank
	private Instant enabledDate;

	public UserRole(long id) {
		super();
		this.id = id;
	}

	public UserRole() {
		super();
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Instant getEnabledDate() {
		return enabledDate;
	}

	public void setEnabledDate(Instant enabledDate) {
		this.enabledDate = enabledDate;
	}

	public Instant getDisabledDate() {
		return disabledDate;
	}

	public void setDisabledDate(Instant disabledDate) {
		this.disabledDate = disabledDate;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Set<AuthRoleEntry> getAuthRoleEntries() {
		return this.role.getAuthRoleEntries();
	}

}
