package it.neeo.concorsiPA.security.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import it.neeo.concorsiPA.security.model.User;

/**
 * 
 * @author enrico.sanna Creation date: 26 Apr 2019 Project: concorsiPA-security
 */
@Repository
public interface UserRepository extends JpaRepository<User, String> {
	Optional<User> findByEmail(String email);

	Optional<User> findByCf(String cf);

	Optional<User> findByUsername(String username);

	Optional<User> findByUsernameOrEmailOrCf(String username, String email, String cf);

	List<User> findByUsernameIn(List<String> usernames);

	Boolean existsByUsername(String username);

	Boolean existsByEmail(String email);

	Boolean existsByCf(String cf);
}
