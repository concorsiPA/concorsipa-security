package it.neeo.concorsiPA.security.model;

import java.io.Serializable;
import java.time.Instant;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;
import javax.validation.constraints.Size;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * 
 * @author enrico.sanna Creation date: 25 Apr 2019 Project: concorsiPA-security
 */
@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(value = { "createdAt", "updatedAt" }, allowGetters = true)
public abstract class BaseAuditEntity implements Serializable {

	private static final long serialVersionUID = 652537025913263422L;

	@CreationTimestamp
	@Column(name = "dtins", nullable = false, updatable = false, columnDefinition = " datetime default NOW()")
	private Instant dtins = Instant.now();

	@Column(name = "utins", nullable = false, updatable = false)
	@Size(max = 50)
	private String utins;

	@UpdateTimestamp
	@Column(name = "dtupd", nullable = true)
	private Instant dtupd;

	@LastModifiedDate
	@Column(name = "utupd", nullable = true)
	@Size(max = 50)
	private String utupd;

	public Instant getDtins() {
		return dtins;
	}

	public void setDtins(Instant dtins) {
		this.dtins = dtins;
	}

	public Instant getDtupd() {
		return dtupd;
	}

	public void setDtupd(Instant dtupd) {
		this.dtupd = dtupd;
	}

	public String getUtins() {
		return utins;
	}

	public void setUtins(String utins) {
		this.utins = utins;
	}

	public String getUtupd() {
		return utupd;
	}

	public void setUtupd(String utupd) {
		this.utupd = utupd;
	}
}