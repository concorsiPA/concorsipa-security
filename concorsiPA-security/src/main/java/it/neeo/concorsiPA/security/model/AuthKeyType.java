package it.neeo.concorsiPA.security.model;

public enum AuthKeyType {
	MENU, 
	DATA_RESOURCE,
	BOOLEAN,
	NUMBER
}
