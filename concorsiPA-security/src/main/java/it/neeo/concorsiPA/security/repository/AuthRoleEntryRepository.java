package it.neeo.concorsiPA.security.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import it.neeo.concorsiPA.security.model.AuthKey;
import it.neeo.concorsiPA.security.model.AuthRoleEntry;
import it.neeo.concorsiPA.security.model.Role;

public interface AuthRoleEntryRepository extends JpaRepository<AuthRoleEntry, Long> {

	Optional<AuthRoleEntry> findById(Long authorityid);

	List<AuthRoleEntry> findByAuthKey(AuthKey authKey);

	List<AuthRoleEntry> findByRole(Role role);

}
