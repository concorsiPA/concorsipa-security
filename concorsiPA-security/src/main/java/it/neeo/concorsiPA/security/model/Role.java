package it.neeo.concorsiPA.security.model;

import java.time.Instant;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

/**
 * 
 * @author enrico.sanna Creation date: 26 Apr 2019 Project: concorsiPA-security
 */
@Entity
@Table(name = "si_roles", uniqueConstraints = { @UniqueConstraint(columnNames = { "code" }) })
public class Role extends BaseAuditEntity {

	private static final long serialVersionUID = 9031023753534493264L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "role_id")
	private long roleId;

	@NotBlank
	@Size(max = 50)
	private String code;

	@NotBlank
	@Size(max = 100)
	private String description;

	private Instant disabledDate;

	@NotBlank
	private Instant enabledDate;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "role_parent")
	Role roleParent;

	@OneToMany(fetch = FetchType.LAZY)
	@JoinColumn(name = "role_id")
	private Set<UserRole> userRoles = new HashSet<>();

	@OneToMany(fetch = FetchType.LAZY)
	@JoinColumn(name = "role_id")
	private Set<AuthRoleEntry> authRoleEntries = new HashSet<>();

	public Role(long roleId) {
		super();
		this.roleId = roleId;
	}

	public Role() {
		super();
	}

	public long getRoleId() {
		return roleId;
	}

	public void setRoleId(long roleId) {
		this.roleId = roleId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Instant getDisabledDate() {
		return disabledDate;
	}

	public void setDisabledDate(Instant disabledDate) {
		this.disabledDate = disabledDate;
	}

	public Instant getEnabledDate() {
		return enabledDate;
	}

	public void setEnabledDate(Instant enabledDate) {
		this.enabledDate = enabledDate;
	}

	public Role getRoleParent() {
		return roleParent;
	}

	public void setRoleParent(Role roleParent) {
		this.roleParent = roleParent;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Set<UserRole> getUserRoles() {
		return userRoles;
	}

	public void setUserRoles(Set<UserRole> userRoles) {
		this.userRoles = userRoles;
	}

	public Set<AuthRoleEntry> getAuthRoleEntries() {
		return authRoleEntries;
	}

	public void setAuthRoleEntries(Set<AuthRoleEntry> authRoleEntries) {
		this.authRoleEntries = authRoleEntries;
	}
}
