package it.neeo.concorsiPA.security.model;

import java.time.Instant;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import org.hibernate.annotations.NaturalId;

/**
 * 
 * @author enrico.sanna Creation date: 26 Apr 2019 Project: concorsiPA-security
 */
@Entity
@Table(name = "si_users", uniqueConstraints = { @UniqueConstraint(columnNames = { "username" }),
		@UniqueConstraint(columnNames = { "email" }), @UniqueConstraint(columnNames = { "cf" }) })
public class User extends BaseAuditEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3189857910645482012L;

	@Id
	@Size(max = 50)
	private String username;

	@NotBlank
	@Size(max = 100)
	private String password;

	@NotBlank
	@Size(max = 100)
	private String screenname;

	@NotBlank
	@Size(max = 16)
	private String cf;

	@NaturalId
	@NotBlank
	@Size(max = 100)
	@Email
	private String email;

	private Instant lastloginDate;

	private Instant passwordExpireDate;

	private boolean enabled = true;

	private Instant dtdisabledDate;

	@Column(name = "badPasswordNumber")
	private int badPasswordNumber = 0;

	private Instant lastbadPwDate;

	@OneToMany(fetch = FetchType.LAZY)
	@JoinColumn(name = "username")
	private Set<UserRole> userRoles = new HashSet<>();

	public User() {
		this.badPasswordNumber=0;
	}

	public User(String username, String password, String screenname, String cf, String email) {
		this.username = username;
		this.password = password;
		this.screenname = screenname;
		this.cf = cf;
		this.email = email;
		this.badPasswordNumber=0;	
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getScreenname() {
		return screenname;
	}

	public void setScreenname(String screenname) {
		this.screenname = screenname;
	}

	public String getCf() {
		return cf;
	}

	public void setCf(String cf) {
		this.cf = cf;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Instant getLastloginDate() {
		return lastloginDate;
	}

	public void setLastloginDate(Instant lastloginDate) {
		this.lastloginDate = lastloginDate;
	}

	public Instant getPasswordExpireDate() {
		return passwordExpireDate;
	}

	public void setPasswordExpireDate(Instant passwordExpireDate) {
		this.passwordExpireDate = passwordExpireDate;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public Instant getDtdisabledDate() {
		return dtdisabledDate;
	}

	public void setDtdisabledDate(Instant dtdisabledDate) {
		this.dtdisabledDate = dtdisabledDate;
	}

	public int getBadPasswordNumber() {
		return badPasswordNumber;
	}

	public void setBadPasswordNumber(int badPasswordNumber) {
		this.badPasswordNumber = badPasswordNumber;
	}

	public Instant getLastbadPwDate() {
		return lastbadPwDate;
	}

	public void setLastbadPwDate(Instant lastbadPwDate) {
		this.lastbadPwDate = lastbadPwDate;
	}

	public Set<UserRole> getUserRoles() {
		return userRoles;
	}

	public void setUserRoles(Set<UserRole> userRoles) {
		this.userRoles = userRoles;
	}
}
