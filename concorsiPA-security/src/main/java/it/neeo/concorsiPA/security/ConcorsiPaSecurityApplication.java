package it.neeo.concorsiPA.security;

import java.util.TimeZone;

import javax.annotation.PostConstruct;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
 
/**
 * 
 * @author enrico.sanna
 * Creation date: 26 Apr 2019
 * Project: concorsiPA-security
 */
@SpringBootApplication
@EntityScan("it.neeo.concorsiPA.security")
public class ConcorsiPaSecurityApplication {

	@PostConstruct
	void init() {
		TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
	}

	public static void main(String[] args) {
		SpringApplication.run(ConcorsiPaSecurityApplication.class, args);
	}

}
