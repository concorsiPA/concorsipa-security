package it.neeo.concorsiPA.security.security;


import it.neeo.concorsiPA.security.model.User;
import it.neeo.concorsiPA.security.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class CustomUserDetailsService implements UserDetailsService {

    @Autowired
    UserRepository userRepository;

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String usernameOrEmailOrCf)
            throws UsernameNotFoundException {
        // Let people login with either username or email or CF
        User user = userRepository.findByUsernameOrEmailOrCf(usernameOrEmailOrCf, usernameOrEmailOrCf, usernameOrEmailOrCf)
                .orElseThrow(() -> 
                        new UsernameNotFoundException("User not found with username or email or Cf: " + usernameOrEmailOrCf)
        );

        return UserPrincipal.create(user, user.getUserRoles().iterator().next());
    }

    // This method is used by JWTAuthenticationFilter
    @Transactional
    public UserDetails loadUserById(String userId) {
        User user = userRepository.findByUsername(userId).orElseThrow(
            () -> new UsernameNotFoundException("User not found with id : " + userId)
        );

        return UserPrincipal.create(user, user.getUserRoles().iterator().next());
    }
}
