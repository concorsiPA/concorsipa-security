package it.neeo.concorsiPA.security.model;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import org.springframework.security.core.GrantedAuthority;

/**
 * 
 * @author enrico.sanna Creation date: 26 Apr 2019 Project: concorsiPA-security
 */
@Entity
@Table(name = "si_authroles", uniqueConstraints = { @UniqueConstraint(columnNames = { "role_id", "authority_id" }) })
public class AuthRoleEntry extends BaseAuditEntity implements GrantedAuthority {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5005023680187796567L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "role_id")
	private Role role;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "authority_id")
	private AuthKey authKey;

	@NotBlank
	@Size(max = 100)
	private String authValue;

	public AuthRoleEntry(long id) {
		super();
		this.id = id;
	}

	public AuthRoleEntry() {
		super();
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public AuthKey getAuthKey() {
		return authKey;
	}

	public void setAuthority(AuthKey authority) {
		this.authKey = authority;
	}

	public String getAuthValue() {
		return authValue;
	}

	public void setAuthValue(String authValue) {
		this.authValue = authValue;
	}

	@Override
	public String getAuthority() {
		return this.authKey.getClassAndMethodName();
	}

}
