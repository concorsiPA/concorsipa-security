package it.neeo.concorsiPA.security.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import it.neeo.concorsiPA.security.model.AuthKey;

public interface AuthKeyRepository extends JpaRepository<AuthKey, Long> {

	Optional<AuthKey> findById(Long authorityid);

	List<AuthKey> findByAuthorityIdIn(List<Long> authorityids);
// I need to retrieve all the keys from a role
//	List<AuthKey> findByRoleIn(List<Role> roles);

	List<AuthKey> findByClassNameOrMethodName(String className, String method_name);

}
